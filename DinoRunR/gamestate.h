#ifndef INFRUN_GAMESTATE_H
#define INFRUN_GAMESTATE_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <list>
#include "machine.h"

class Machine;

class GameState
{
public:
    GameState() {}
    virtual ~GameState() {};
    virtual void OnEnter(Machine &context) = 0;
    virtual Machine::StateId OnProcess(sf::RenderWindow& window) { return Machine::StateId::HELLO; };
    virtual void OnExit(Machine &context) = 0;

    virtual void Draw(sf::RenderWindow& window) = 0;
};
#endif //INFRUN_GAMESTATE_H
