#ifndef MACHINE_H
#define MACHINE_H

#include <map>
#include "SFML/Graphics.hpp"

class GameState;

class Machine
{
public:
    enum class StateId { HELLO, RUNNING };

    Machine();
    ~Machine();

    void Run();



protected:

    void SetState(StateId nextState);
    bool ShouldStillRun();

    bool running;
    StateId state;
    std::map<StateId, GameState*> states;
    sf::RenderWindow window;
    sf::View view;
};

#endif
