#include "Running.h"
#include "dino.h"

Running::Running() 
{

	background = sf::RectangleShape();
	background.setFillColor(sf::Color(200, 200, 200, 255));
	background.setSize(sf::Vector2f(850.f, 500.f));

	blob = new sf::CircleShape(2.f);
	blob->setFillColor(sf::Color::Red);
	blobX = 50.f;
	blobY = 50.f;
	blobSpeed = 5.f;

	ground = sf::RectangleShape();
	ground.setSize(sf::Vector2f(850.f, 200.f));
	ground.setFillColor(sf::Color(150, 150, 150, 255));
	ground.setPosition(0, 300);
}

void Running::OnEnter(Machine& context)
{
	std::cout << "Running, OnEnter!" << std::endl;
}

Machine::StateId Running::OnProcess(sf::RenderWindow& window)
{
	// Handle input
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) 
	{
		std::cout << "Lets get back to HELLO" << std::endl;
		return Machine::StateId::HELLO;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		dino.Jump();
		//blobY -= blobSpeed;
	}
	
	// Debug blob for position check
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) 
	{
		blobX -= blobSpeed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) 
	{
		blobX += blobSpeed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) 
	{
		blobY -= blobSpeed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) 
	{
		blobY += blobSpeed;
	}
	// Move objects
	blob->setPosition(blobX, blobY);


	// Process entities
	dino.Process();

	return Machine::StateId::RUNNING;
}

void Running::OnExit(Machine& context)
{
	std::cout << "Running, OnExit!" << std::endl;
}

void Running::Draw(sf::RenderWindow& window)
{
	window.draw(background);
	window.draw(ground);
	window.draw(*blob);

	dino.Draw(window);
}
