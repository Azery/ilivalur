#pragma once
#include "gamestate.h"
#include "machine.h"
#include "dino.h"
#include <SFML/Graphics.hpp>
class Running :
	public GameState
{
public:
	Running();
	// Inherited via GameState
	void OnEnter(Machine& context) override;
	Machine::StateId OnProcess(sf::RenderWindow& window) override;
	void OnExit(Machine& context) override;
	void Draw(sf::RenderWindow& window) override;

protected:
	sf::RectangleShape background;
	std::list<sf::Drawable*> obstacles;

	sf::CircleShape* blob;
	float blobX;
	float blobY;
	float blobSpeed;

	sf::RectangleShape ground;
	dino dino;
};

