#include <SFML/Graphics.hpp>
#include "machine.h"
#include "states/Hello.h"
#include "Running.h"
#include <iostream>
#include <list>

Machine::Machine()
{
	// Start the machine running
	running = true;

	// Start in the Hello state
	state = StateId::HELLO;

	// Create a window we can draw to
	window.create(sf::VideoMode(850, 500), "SFML works!");
	
	// Enable vertical sync - prevents tearing and locks framerate to display
	window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(60);

	// Add states to the state map
	states.emplace(StateId::HELLO, new Hello());
	states.emplace(StateId::RUNNING, new Running());
	// states.emplace(StateId::EXIT, new StateExit());
}

Machine::~Machine()
{
	// Free the states from memory
	for (auto state : states)
		delete state.second;

	states.clear();
}

void Machine::Run()
{
	while (ShouldStillRun()) 
	{
		window.clear();

		// Process current state and get an indication if we should continue drawing or change state
		StateId nextState = states[state]->OnProcess(window);

		if (nextState == state) 
		{
			states[state]->Draw(window);

			window.display();
		}
		else 
		{
			SetState(nextState);
		}
	}
}

bool Machine::ShouldStillRun() 
{
	sf::Event event;

	while (window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			window.close();
			return false;

		case sf::Event::KeyReleased:
			// Reload map on F5
			if (event.key.code == sf::Keyboard::F5)
			{
				// Todo make restart function
			}

			// Exit program on escape
			if (event.key.code == sf::Keyboard::Escape)
			{
				//window.close();
				//return false;
			}
			break;

		default:
			// Ignore the other events
			break;
		}
	}
	return true;
}

void Machine::SetState(StateId nextState)
{
	states[state]->OnExit(*this);
	this->state = nextState;
	states[state]->OnEnter(*this);
}

