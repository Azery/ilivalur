#include "Hello.h"
#include "../machine.h"
#include <iostream>
#include <list>
#include <SFML/Graphics.hpp>


Hello::Hello()
{
	if (!font.loadFromFile("assets/consola.ttf")) {
		std::cout << "font.loadFromFile failed" << std::endl;
	}

	gameTitle = sf::Text();
	gameTitle.setString("DinoRunR");
	gameTitle.setCharacterSize(50);
	gameTitle.setPosition(300, 100);
	gameTitle.setFillColor(sf::Color::Blue);
	gameTitle.setFont(font);

	description = sf::Text();
	description.setString("Press space to start jumping");
	description.setCharacterSize(20);
	description.setPosition(250, 200);
	description.setFillColor(sf::Color::Blue);
	description.setFont(font);

	background = sf::RectangleShape();
	background.setFillColor(sf::Color(200, 200, 200, 255));
	background.setSize(sf::Vector2f(850.f, 500.f));

}

void Hello::OnEnter(Machine& machine) 
{
	std::cout << "Hello, OnEnter!" << std::endl;
}

Machine::StateId Hello::OnProcess(sf::RenderWindow& window) 
{
	// Handle input
	// Swap to event key released instead
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		std::cout << "Lets start RUNNING" << std::endl;
		return Machine::StateId::RUNNING;
	}

	return Machine::StateId::HELLO;
}

void Hello::OnExit(Machine& machine) 
{
	std::cout << "Hello, OnExit!" << std::endl;
}

void Hello::Draw(sf::RenderWindow& window) 
{
	window.draw(background);
	window.draw(gameTitle);
	window.draw(description);

}
