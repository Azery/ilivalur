
#include "../gamestate.h"
#include "../machine.h"
#include <iostream>
#include <list>
#ifndef INFRUN_HELLO_H
#define INFRUN_HELLO_H



class Hello : public GameState {
public:
	Hello();
	Machine::StateId OnProcess(sf::RenderWindow& window) override;
	void OnEnter(Machine& machine) override;
	void OnExit(Machine& machine) override;
	//std::list<sf::Drawable*> GetDrawables() override;

	void Draw(sf::RenderWindow& window) override;

protected:

	sf::RectangleShape background;

	sf::Text gameTitle;
	sf::Text description;

	// Font has to live while description is rendered
	sf::Font font;
};

#endif //INFRUN_HELLO_H
