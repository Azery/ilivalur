#include "dino.h"
#include <iostream>
#include <math.h>


dino::dino()
{
	tempDino = sf::RectangleShape();
	tempDino.setSize(sf::Vector2f(20.f, 50.f));
	tempDino.setFillColor(sf::Color::Blue);

	x = 100;
	y = dinoGround;
}

void dino::Process()
{
	// std::cout << "dino::Process, x: " << x << " y: " << y << " velocity: " << velocity << " isJumping:" << isJumping << std::endl;

	// Handle jump logic
	if (isJumping)
	{
		velocity -= gravity;
		y -= round(velocity);

		if (y > dinoGround)
		{
			y = dinoGround;
			velocity = 0;
			isJumping = false;

			// For debug purpose
			tempDino.setFillColor(sf::Color::Blue);
		}

		tempDino.setPosition(x, y);
	}
}

void dino::Draw(sf::RenderWindow& window)
{
	//std::cout << "dino::Draw" << std::endl;

	window.draw(tempDino);
}


void dino::Jump()
{
	std::cout << "dino::Jump" << std::endl;

	// For debug purpose
	tempDino.setFillColor(sf::Color::Green);

	if (!isJumping)
	{
		isJumping = true;
		velocity = 20;
	}
}
