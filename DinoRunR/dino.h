#pragma once
#include "entity.h"
#include <SFML/Graphics.hpp>

class dino :
    public entity
{
public:
    dino();
    void Process() override;
    void Draw(sf::RenderWindow& window) override;
    void Jump();
protected:
    bool isJumping = false;
    float gravity = 1.f; 
    float velocity = 0.f;

    // Need to subtract the height of the dino from ground position
    int dinoGround = ground - 50;

    sf::RectangleShape tempDino;
};

